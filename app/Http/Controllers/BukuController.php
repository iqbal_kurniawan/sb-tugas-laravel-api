<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

class BukuController extends Controller
{
    public function store(Request $request){
    	DB::table('ms_buku')->insert([
		    [
		    	'kode_buku' => request('kode_buku'), 
		    	'judul' => request('judul'),
		    	'pengarang' => request('pengarang'),
		    	'tahun_terbit'=>request('tahun_terbit'),
		    	'users_id'=>request('users_id')
		    ],
		]);

		  return response('Data Berhasil Di Tambahkan');
    }

    public function index(){
    	$buku = DB::table('ms_buku')->select('*')->get();
    	 return response($buku);
    }

}
