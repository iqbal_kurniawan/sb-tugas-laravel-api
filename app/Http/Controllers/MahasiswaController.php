<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

class MahasiswaController extends Controller
{
	 public function store(Request $request){
    	DB::table('ms_mahasiswa')->insert([
		    [
		    	'nim' => request('nim'), 
		    	'nama' => request('nama'),
		    	'fakultas' => request('fakultas'),
		    	'jurusan'=>request('jurusan'),
		    	'nohp'=>request('nohp'),
		    	'nowa'=>request('nowa'),
		    	 'users_id'=>request('users_id')
		    ],
		]);

		  return response('Data Berhasil Di Tambahkan');
    }

    public function index(){
    	$mahasiswa = DB::table('ms_mahasiswa')->select('*')->get();
    	 return response($mahasiswa);
    }

    public function detail($id){
    	   $mahasiswa = DB::table('ms_mahasiswa')->where('users_id',$id)->get();
    	   return response($mahasiswa);
    }
}
