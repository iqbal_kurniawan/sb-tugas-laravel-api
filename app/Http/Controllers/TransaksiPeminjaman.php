<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

class TransaksiPeminjaman extends Controller
{
     public function store(Request $request){
    	DB::table('tr_peminjaman_buku')->insert([
		    [
		    	'tgl_pinjam' => request('tgl_pinjam'), 
		    	'tgl_max_pinjam' => request('tgl_max_pinjam'),
		    	'tgl_kembali' => request('tgl_kembali'),
		    	'status_ontime'=>request('status_ontime'),
		    	'nim'=>request('nim'),
		    	'kode_buku'=>request('kode_buku')
		    ],
		]);

		  return response('Data Berhasil Di Tambahkan');
    }

    public function index(){
    	$peminjaman = DB::table('tr_peminjaman_buku')->select('*')->get();
    	 return response($peminjaman);
    }
}
