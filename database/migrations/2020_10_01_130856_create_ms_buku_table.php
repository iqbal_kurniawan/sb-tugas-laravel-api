<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMsBukuTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ms_buku', function (Blueprint $table) {
            $table->string('kode_buku',85);
            $table->primary('kode_buku');
            $table->string('judul',150);
            $table->string('pengarang',150);
            $table->year('tahun_terbit');
            $table->integer('users_id')->unsigned();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ms_buku');
    }
}
