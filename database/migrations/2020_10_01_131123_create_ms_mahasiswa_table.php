<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMsMahasiswaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ms_mahasiswa', function (Blueprint $table) {
            $table->string('nim',100);
            $table->primary('nim');
            $table->string('nama',150);
            $table->string('fakultas',150);
            $table->string('jurusan',150);
            $table->string('nohp',50);
            $table->string('nowa',45);
            $table->integer('users_id')->unsigned();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ms_mahasiswa');
    }
}
