<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTrPeminjamanBukuTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tr_peminjaman_buku', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->date('tgl_pinjam');
            $table->date('tgl_max_pinjam');
            $table->date('tgl_kembali');
            $table->boolean('status_ontime');
            $table->string('nim',100);
            $table->string('kode_buku',85);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tr_peminjaman_buku');
    }
}
