<?php
Route::namespace('Auth')->group(function(){
	Route::post('register','RegisterController');
	Route::post('login','LoginController');
	Route::post('logout','LogoutController');

});

Route::group(['middleware' => 'auth:api'], function(){
	
	Route::group(['prefix'=>'admin','middleware'=> 'role-admin'] ,function(){

		Route::post('/buku','BukuController@store');
		Route::get('/buku','BukuController@index');

		Route::post('/mahasiswa','MahasiswaController@store');
		Route::get('/mahasiswa','MahasiswaController@index');

		Route::post('/peminjaman-buku','TransaksiPeminjaman@store');
		Route::get('/peminjaman-buku','TransaksiPeminjaman@index');

	});

	Route::group(['prefix'=>'user','middleware'=> 'role-user'] ,function(){

		Route::get('/mahasiswa/{id}','MahasiswaController@detail');
		Route::get('/buku','BukuController@index');
		

	});


});

// Route::get('/user','UserController');
Route::get('/test',function(){
	echo 'testing';
});
